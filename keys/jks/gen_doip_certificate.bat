keytool -genkey -keyalg RSA -keysize 2048 -validity 365 -keypass 123456 -keystore doip_service.keystore -storepass 123456 -dname "UID=86.5000.470/doip.localTLSService"
keytool -export -file doip_service_pub.cer -keystore doip_service.keystore -storepass 123456

//generate keystore for 86.5000.470/dou.TEST
keytool -genkey -keyalg RSA -keysize 2048 -validity 365 -keypass 123456 -keystore dou.TEST.keystore -storepass 123456 -dname "UID=86.5000.470/dou.TEST"
keytool -export -file dou.TEST_pub.cer -keystore dou.TEST.keystore -storepass 123456

//generate keystore for 86.5000.470/dou.SUPER
keytool -genkey -keyalg RSA -keysize 2048 -validity 365 -keypass 123456 -keystore dou.SUPER.keystore -storepass 123456 -dname "UID=86.5000.470/dou.SUPER"
keytool -export -file dou.SUPER.cer -keystore dou.SUPER.keystore -storepass 123456


//convert 86.5000.470/dou.TEST for android
keytool -importkeystore -srckeystore dou.TEST.keystore -destkeystore dou.TEST.keystore.bks -srcstoretype JKS -deststoretype BKS -srcstorepass 123456 -deststorepass 123456 -provider org.bouncycastle.jce.provider.BouncyCastleProvider


//generate keystore for cordra
keytool -genkey -keyalg RSA -keysize 2048 -validity 365 -keypass 123456 -keystore dou.cordra.keystore -storepass 123456
keytool -export -file dou.cordra.cer -keystore dou.cordra.keystore -storepass 123456