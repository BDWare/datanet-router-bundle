## 接口文档



### GRSRouter 

|        名称        | 接口功能                                 |         请求参数          |   请求参数说明   |              返回类型               |                  返回说明                  |
| :----------------: | ---------------------------------------- | :-----------------------: | :--------------: | :---------------------------------: | :----------------------------------------: |
|    generateKey     | 根据doi生成jwk                           |        String  doi        |       doi        |           ResponseWrapper           |                    jwk                     |
|      getInfo       | 根据doi得到organization                  |        String doi         |       doi        |    ResponseWrapper<Organization>    |                organization                |
|   generatePrefix   | 生成前缀                                 |           null            |       null       |           ResponseWrapper           | UUID.randomUUID().toString()生成唯一识别码 |
|      saveInfo      | 保存organization                         | Organization organization |   organization   |           ResponseWrapper           |                  保存成功                  |
|     deleteOrg      | 删除organization                         | Organization organization |   organization   |           ResponseWrapper           |                  删除成功                  |
|      approve       | 改变organization状态为审核通过并保存 | Organization organization |   organization   |           ResponseWrapper           |                  保存成功                  |
|     disapprove     | 改变organization状态为待审核并保存   | Organization organization |   organization   |           ResponseWrapper           |                  保存成功                  |
|   applyForPrefix   | 改变organization状态为待审核并保存   | Organization organization |   organization   |           ResponseWrapper           |                  保存成功                  |
| updateOrganization | 更新organization                         | Organization organization |   organization   |           ResponseWrapper           |                  保存成功                  |
|  allOrganizations  | 获取所有organization                     |           null            |       null       | ResponseWrapper<List<Organization>> |                organization                |
|      lrsJoin       | LRS启动后连接GRS，更新信息               | Organization organization | LRS organization |           ResponseWrapper           |                 update结果                 |
|      resolve       | 解析前缀，返回信息                       |        String doi         |       doi        |           ResponseWrapper           |               StateInfoBase                |
|                    |                                          |                           |                  |                                     |                                            |



```json
名称: 
generateKey

请求示例: 
123456

返回示例:
{
  "success":true,
  "data":"{\"kty\":\"EC\",\"d\":\"_HsVnRu5kdwNUo6SMs5kjkRu7VHo4NEQURjuQIt9cSc\",\"use\":\"sig\",\"crv\":\"P-256\",\"kid\":\"123456\",\"x\":\"oIgT29eg0tzu398IcZsjBF99TTk93Lf3laMfn1V9Oqg\",\"y\":\"254SHZtpCGFHMWs5CvylkjsJtoGE2u2D55L1vAeU7Tw\",\"alg\":\"ES256\"}",
  "errorMessage":"",
  "showType":0
}

名称：
generatePrefix

请求示例：
null

返回示例：
{
  "success":true,
  "data":"298b8226-888e-4268-bb03-51d818353a03",
  "errorMessage":"",
  "showType":0
}

名称：

请求示例：

返回示例：

```



### LRSRouter 

|        名称         | 接口功能                                 |         请求参数          | 请求参数说明 |              返回类型               |                  返回说明                  |
| :-----------------: | ---------------------------------------- | :-----------------------: | :----------: | :---------------------------------: | :----------------------------------------: |
|   generateSuffix    | 生成后缀，可以自定义后缀生成规则         |        String type        | type，如dou  |           ResponseWrapper           |     type+UUID.randomUUID().toString()      |
|      getPrefix      | 获取IRS的前缀信息                        |           null            |     null     |           ResponseWrapper           |              irsConfig.prefix              |
|       resolve       | 解析对应的doi                            |        String doi         |     doi      |           ResponseWrapper           |               StateInfoBase                |
|   getCurrentUser    | 解析doi得到user                          |        String doi         |     doi      |           ResponseWrapper           |                    User                    |
|    registerUser     | 注册user                                 |         User user         |     user     |           ResponseWrapper           |                注册是否成功                |
|     updateUser      | 更新user信息                             |         User user         |     user     |           ResponseWrapper           |            更新用户信息是否成功            |
|   getRepoAndRegs    | 获取repos                                |           null            |     null     |           ResponseWrapper           |               StateInfoBases               |
|     getAllUsers     | 获取users                                |           null            |     null     |           ResponseWrapper           |                 userStats                  |
|      getAllDos      | 获取dos                                  |           null            |     null     |           ResponseWrapper           |               StateInfoBases               |
|      getValue       |                                          |        String key         |     key      |           ResponseWrapper           |                                            |
|    *generateKey     | 根据doi生成jwk                           |        String  doi        |     doi      |           ResponseWrapper           |                    jwk                     |
|      *getInfo       | 根据doi得到organization                  |        String doi         |     doi      |    ResponseWrapper<Organization>    |                organization                |
|   *generatePrefix   | 生成前缀                                 |           null            |     null     |           ResponseWrapper           | UUID.randomUUID().toString()生成唯一识别码 |
|      *saveInfo      | 保存organization                         | Organization organization | organization |           ResponseWrapper           |                  保存成功                  |
|      *approve       | 改变organization状态为审核通过并保存 | Organization organization | organization |           ResponseWrapper           |                  保存成功                  |
|     *disapprove     | 改变organization状态为待审核并保存   | Organization organization | organization |           ResponseWrapper           |                  保存成功                  |
|   *applyForPrefix   | 改变organization状态为待审核并保存   | Organization organization | organization |           ResponseWrapper           |                  保存成功                  |
| *updateOrganization | 更新organization                         | Organization organization | organization |           ResponseWrapper           |                  保存成功                  |
|  *allOrganizations  | 获取所有organization                     |           null            |     null     | ResponseWrapper<List<Organization>> |                organization                |
|       getStat       | 返回irs中的统计信息                      |           null            |     null     |           ResponseWrapper           |                    Stat                    |
|                     |                                          |                           |              |                                     |                                            |



```json
名称: 
generateSuffix

请求示例: 
dou

返回示例:
{
  "success":true,
  "data":"dou.56de7db2-4559-42a3-adaf-ca65f61ed3e0",
  "errorMessage":"",
  "showType":0
}


名称：

请求示例：

返回示例：

```



### LoginUserRouter 

|     名称      | 接口功能 |        请求参数         | 请求参数说明 |    返回类型     |   返回说明   |
| :-----------: | -------- | :---------------------: | :----------: | :-------------: | :----------: |
|     login     | 登陆     |   LoginUser loginUser   |  loginUser   | ResponseWrapper |     jwk      |
| getCurrentUse |          | String doi, String role |  doi，role   | ResponseWrapper | organization |
|  authWithJWT  |          |          null           |     null     |     boolean     |    false     |
|    Scrypto    |          |    Crypto cryptoData    |  cryptoData  |     String      |  公钥或私钥  |



## 数据结构

#### organization

```java
//前缀信息，存储前缀对应的机构信息
public class Organization {

    private Long id;
    //owner的doi号
    String doi;
    String name;
    String jwkKey;
    String protocol;
    String serviceAddress;
    String status;
    //分配的前缀
    String prefix;

    public Organization();	// 几个重载的 Organization 方法
    
        //将信息转为标桩格式的状态信息
    public static Organization fromStateInfoBase(StateInfoBase stateInfoBase);
    
        //将信息转为标桩格式的状态信息
    public StateInfoBase toStateInfoBase();
    
    // 一堆 get 或 set 方法
}
```



#### User

```java
public class User {

    private Long id;
    private String doi;
    private String suffix;
    private String organization;
    private String publicKey;
    private String lastModified;
    private String des;


    public UserStateInfo toUserStateInfo()；

    public User()；  // 几个重载的 User 方法
        
    // 一堆get或set方法
    
    public void update(User user)    
}    
```



#### LoginUser

```java
public class LoginUser {
    String doi;
    // header.payload.org.bdware.router.deprecated.SignatureTest "eyJhbGciOiJFUzI1NiIsImtpZCI6IjEyMzQ1NiJ9.aGVsbG8.UmGZzEhI8WPhu-FEu4IPRmCJpClTlPC-yYpVt5YW0GMLbKDlTVoG7QWyILWEsblHdAqkRHieRNnSVxySbl8pvA"
    String token;
    String source;
    String role;

    public LoginUser();	  // 几个重载的 LoginUser 方法
    
   // 一堆get或set方法
}
```



#### StateInfoBase

```java
public class StateInfoBase {
    public String identifier;
    public JsonObject handleValues;
    static final String UUID_KEY = "UUID";

    public StateInfoBase();
    ...
}    
```





