package org.bdware.router;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.Scanner;

public class DeploySampler {
    static class Config {
        String samplerJarPath;
        String samplerLibPath;
        String targetPath;
    }

    public static void main(String[] args) throws IOException {
        Config config = new Gson().fromJson(new FileReader("./input/deploySampler.json"), Config.class);
        //clear
        File targetDir = new File(config.targetPath);
        File[] files = targetDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.getName().startsWith("Apache"))
                    return false;
                if (!pathname.getName().endsWith(".jar"))
                    return false;
                return true;
            }
        });
        if (files != null)
            for (File f : files) f.delete();
        //copy
        FileUtils.copyFileToDirectory(new File(config.samplerJarPath), targetDir);
        files = new File(config.samplerLibPath).listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".jar");
            }
        });
        if (files != null)
            for (File f : files) {
                FileUtils.copyFileToDirectory(f, targetDir);
            }
        ProcessBuilder pb = new ProcessBuilder();
        File binDir = new File(new File(config.targetPath).getParentFile().getParentFile(), "bin");
        pb.directory(binDir);
        pb.command("sh", "jmeter");
        Process process = pb.start();
        trace(process.getInputStream(), System.out);
        trace(process.getErrorStream(), System.err);
        try {
            Thread.sleep(365L * 24L * 3600L * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void trace(InputStream inputStream, OutputStream outputStream) {
        new Thread() {
            public void run() {
                Scanner sc = new Scanner(inputStream);
                PrintStream printer = new PrintStream(outputStream);
                for (String str; sc.hasNextLine(); ) {
                    str = sc.nextLine();
                    printer.println(str);
                }
            }
        }.start();
    }
}
