package org.apache.jmeter.protocol.doa.sampler;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.jmeter.samplers.AbstractSampler;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.TestStateListener;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.bdware.doip.audit.AuditDoaClient;
import org.bdware.doip.audit.EndpointConfig;
import org.bdware.doip.audit.client.AuditDoipClient;
import org.bdware.doip.audit.client.AuditIrpClient;
import org.bdware.doip.codec.digitalObject.DigitalObject;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.doip.codec.doipMessage.DoipMessageFactory;
import org.bdware.doip.codec.doipMessage.DoipResponseCode;
import org.bdware.doip.codec.exception.DoDecodeException;
import org.bdware.doip.codec.operations.BasicOperations;
import org.bdware.doip.dbrepo.DBTableDO;
import org.bdware.doip.endpoint.client.ClientConfig;
import org.bdware.doip.endpoint.client.DoipMessageCallback;
import org.bdware.doip.httprepo.HttpDO;
import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.irplib.exception.IrpConnectException;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicInteger;

public class DoaSampler extends AbstractSampler implements TestStateListener {
    private static final long serialVersionUID = 1L;
    private static final String DOA_AGREEMENT = "doip.agreement";
    private static final String DOA_IP = "doip.ip";
    private static final String DOA_PORT = "doip.port";
    private static final String DOA_PUBKEY = "doip.pubkey";
    private static final String DOA_PRIKEY = "doip.prikey";
    private static final String DOA_METHOD = "doip.method";

    private static final String DOA_REPONAME = "doip.reponame";
    private static final String DOA_REPOID = "doip.repoid";
    private static final String DOA_INITIALVALUE = "0";
    private static final String DOA_BODY = "doip.body";
    private static final Logger log = LoggingManager.getLoggerForClass();

    public DoaSampler() {
        setName("DOA sampler");
    }

    ThreadLocal<AuditIrpClient> irpClientThreadLocal = new ThreadLocal<>();
    ThreadLocal<AuditDoipClient> doipClientThreadLocal = new ThreadLocal<>();

    @Override
    public SampleResult sample(Entry entry) {
        SampleResult result = new SampleResult();
        result.setSampleLabel(getName());
        try {
            result.sampleStart();
            result.setRequestHeaders("服务器地址：" + getDoaAgreement() + "://" + getDoaIp() + ":" + getDoaPort() +
                    '\n' + "请求公钥：" + getDoaPubkey() + '\n' + "请求私钥：" + getDoaPrikey() + '\n' + "请求方式：" +
                    getDoaMethod() + '\n' + "请求参数：" + getDoaBody() + '\n' + "仓库名称：" + getDoaReponame() + '\n' + "仓库ID：" + getDoaRepoid())
            ;
            String val = rundoa();
            if (val != null && val.length() != 0) {
                result.setResponseData(val, "UTF-8");
                result.sampleEnd();
                result.setSuccessful(true);
                result.setResponseCodeOK();
            } else {
                result.sampleEnd(); // stop stopwatch
                result.setSuccessful(false);
                java.io.StringWriter stringWriter = new java.io.StringWriter();
                result.setResponseData(stringWriter.toString(), null);
                result.setDataType(org.apache.jmeter.samplers.SampleResult.TEXT);
                result.setResponseCode("FAILED");
            }
            //result.setResponseData(val.getBytes());

        } catch (Exception e) {
            result.sampleEnd(); // stop stopwatch
            result.setSuccessful(false);
            result.setResponseMessage("Exception: " + e);
            java.io.StringWriter stringWriter = new java.io.StringWriter();
            e.printStackTrace(new java.io.PrintWriter(stringWriter));
            result.setResponseData(stringWriter.toString(), null);
            result.setDataType(org.apache.jmeter.samplers.SampleResult.TEXT);
            result.setResponseCode("FAILED");
        }
        return result;
    }

    private String rundoa() throws IrpClientException {
        JsonObject doa_respose = new JsonObject();
        AuditIrpClient irp2client;

        String url = getDoaAgreement() + "://" + getDoaIp() + ":" + getDoaPort();
        String xieyi = getDoaMethod();
        String val = "";
        if (xieyi.equals("dbretrieve")) {
            AuditDoipClient dbclient = getOrCreateDoipClient(url);
            String doid = getjsonstr("doid");
            String offset = getjsonstr("offset");
            String count = getjsonstr("count");
            DoipMessage retrieveMsg = DBTableDO.retrieveMsg(doid, Integer.parseInt(offset), Integer.parseInt(count));
            if (!dbclient.isConnected()) {
                log.error("dbclient disconnected:" + allocDoipClient.get() + " -> " + dbclient.getRepoUrl());
                try {
                    dbclient.reconnect();
                    allocDoipClient.incrementAndGet();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            DoipMessage resp = dbclient.sendMessageSync(retrieveMsg);
            try {
                DigitalObject digitalObject = resp.body.getDataAsDigitalObject();
                String str = new String(digitalObject.elements.get(0).getData(), StandardCharsets.UTF_8);
                val = "data：" + str;
            } catch (DoDecodeException e) {
                e.printStackTrace();
            } catch (Exception e) {
                log.error("parseDO Error:" + resp.header.parameters.response.getName() + "->" + resp.header.parameters.attributes.toString());
            }
        } else if (xieyi.equals("apiretrieve")) {
            AuditDoipClient dbclient = getOrCreateDoipClient(url);
            String doid = getjsonstr("doid");
            DoipMessage retrieveMsg = HttpDO.retrieveMsg(doid, "", new JsonObject(), new byte[0]);
            if (!dbclient.isConnected()) {
                log.error("dbclient disconnected:" + allocDoipClient.get() + " -> " + dbclient.getRepoUrl());
                try {
                    dbclient.reconnect();
                    allocDoipClient.incrementAndGet();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            DoipMessage resp = dbclient.sendMessageSync(retrieveMsg);
            try {
                if (!resp.header.parameters.response.equals(DoipResponseCode.Success)) {
                    log.error("failed!" + resp.header.parameters.response.getName() + "->" + resp.header.parameters.attributes.toString());
                    return "failed!" + resp.header.parameters.response.getName() + "->" + resp.header.parameters.attributes.toString();
                }
                HttpDO digitalObject = HttpDO.asHttpDO(resp.body.getDataAsDigitalObject());
//                MessageHeader heade = resp.header;
//                doa_respose.addProperty("healde", String.valueOf(heade));
//                doa_respose.addProperty("body", resp.body.getDataAsJsonString());
                String str = digitalObject.getBodyAsString();
                val = "data：" + str;
            } catch (DoDecodeException e) {
                e.printStackTrace();
            } catch (Exception e) {
                log.error("parseDO Error:" + resp.header.parameters.response.getName() + "->" + resp.header.parameters.attributes.toString());
            }

        } else if (xieyi.equals("fileretrieve")) {
            AuditDoipClient dbclient = getOrCreateDoipClient(url);
            String doid = getjsonstr("doid");
            DoipMessageFactory.DoipMessageBuilder builder = new DoipMessageFactory.DoipMessageBuilder();
            builder.createRequest(doid, BasicOperations.Retrieve.getName());
            DoipMessage retrieveMsg = builder.create();
            DoipMessage resp = dbclient.sendMessageSync(retrieveMsg);
            try {
                String data = new String(resp.body.encodedData, StandardCharsets.UTF_8);
                val = "data: " + data;
            } catch (Exception e) {
                log.error("parseDO Error:" + resp.header.parameters.response.getName() + "->" + resp.header.parameters.attributes.toString());
                e.printStackTrace();
            }
        } else if (xieyi.equals("createdo")) {
            AuditDoipClient doip2client;
            doip2client = new AuditDoipClient();
            ClientConfig config = new ClientConfig(url);
            doip2client.connect(config);
            String repoId = getDoaRepoid();
            String dbUrl = getjsonstr("dburl");
            String tableName = getjsonstr("tablename");
            String userName = getjsonstr("username");
            String password = getjsonstr("password");
            AtomicInteger i = new AtomicInteger(Integer.parseInt(getDoaInitialvalue()));
            DBTableDO dbTableDO = DBTableDO.createDO(dbUrl, tableName, userName, password);
            DoipMessage resp = doip2client.createSync(repoId, dbTableDO);
            val = "doId：" + resp.header.parameters.id;
            doip2client.close();
        } else if (xieyi.equals("register")) {
            irp2client = getOrCreateIrp2CLientTest();
            StateInfoBase info = new StateInfoBase();
            info.handleValues = new JsonObject();
            info.handleValues.addProperty("repoId", getDoaRepoid());
            info.handleValues.addProperty("other", getDoaBody());
            val = irp2client.register(info);
        } else if (xieyi.equals("resolve")) {
            //IrpRequestFactory factory;
            irp2client = getOrCreateIrp2CLientTest();
            String doid2 = getjsonstr("doid");
            val = new Gson().toJson(irp2client.resolve(doid2));
        } else if (xieyi.equals("resolvedirectly")) {
            //IrpRequestFactory factory;
            irp2client = getOrCreateIrp2CLientTest();
            String doid2 = getjsonstr("doid");
            val = new Gson().toJson(irp2client.resolveDirectly(doid2));
        } else if (xieyi.equals("retrievedoa")) {
            final AtomicInteger inter = new AtomicInteger(Integer.parseInt(getDoaInitialvalue()));
            EndpointConfig config = new EndpointConfig();
            config.routerURI = url;
            AuditDoaClient clientdoa = new AuditDoaClient(config);
            clientdoa.retrieve(getjsonstr("doid"), getjsonstr("element"), true, new DoipMessageCallback() {
                @Override
                public void onResult(DoipMessage msg) {
                    log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" + msg.header.parameters.id + "AAAAA" + msg.body.getDataAsJsonString());
                    //val = msg.header.parameters.id;
                    //val =inter.incrementAndGet();
                }
            });
            clientdoa.close();
        } else {
            log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$进入了else");
        }
        return val;
    }

    static AtomicInteger allocDoipClient = new AtomicInteger(0);

    private AuditDoipClient getOrCreateDoipClient(String url) {
        AuditDoipClient dbclient = doipClientThreadLocal.get();
        if (dbclient == null) {
            dbclient = new AuditDoipClient();
            ClientConfig config = new ClientConfig(url);
            dbclient.connect(config);
            allocDoipClient.incrementAndGet();
            doipClientThreadLocal.set(dbclient);
            return dbclient;
        }
        if (!dbclient.isConnected()) {
            try {
                dbclient.reconnect();
                allocDoipClient.incrementAndGet();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dbclient;
    }

    private AuditIrpClient getOrCreateIrp2CLientTest() {
        AuditIrpClient ret = irpClientThreadLocal.get();
        if (ret == null) {
            String url = getDoaAgreement() + "://" + getDoaIp() + ":" + getDoaPort();
            EndpointConfig config = new EndpointConfig();
            config.routerURI = url;
            config.repoName = getDoaReponame();
            config.publicKey = getDoaPubkey();
            config.privateKey = getDoaPrikey();
            ret = new AuditIrpClient(config);
            irpClientThreadLocal.set(ret);
        }
        try {
            ret.reconnect();
        } catch (IrpConnectException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String getjsonstr(String param) {
        JsonObject json = JsonParser.parseString(getDoaBody()).getAsJsonObject();
        //System.out.println(json.getString(param));
        return json.get(param).getAsString();
    }

    public String getDoaAgreement() {
        return getPropertyAsString(DOA_AGREEMENT);
    }

    public void setDoaAgreement(String doip_agreement) {
        setProperty(DOA_AGREEMENT, doip_agreement);
    }

    public String getDoaIp() {
        return getPropertyAsString(DOA_IP);
    }

    public void setDoaIp(String doip_ip) {
        setProperty(DOA_IP, doip_ip);
    }

    public String getDoaPort() {
        return getPropertyAsString(DOA_PORT);
    }

    public void setDoaPort(String doip_port) {
        setProperty(DOA_PORT, doip_port);
    }

    public String getDoaPubkey() {
        return getPropertyAsString(DOA_PUBKEY);
    }

    public void setDoaPubkey(String pubkey) {
        setProperty(DOA_PUBKEY, pubkey);
    }

    public String getDoaPrikey() {
        return getPropertyAsString(DOA_PRIKEY);
    }

    public void setDoaPrikey(String prikey) {
        setProperty(DOA_PRIKEY, prikey);
    }

    public String getDoaMethod() {
        return getPropertyAsString(DOA_METHOD);
    }

    public void setDoaMethod(String doip_method) {
        setProperty(DOA_METHOD, doip_method);
    }

    public String getDoaReponame() {
        return getPropertyAsString(DOA_REPONAME);
    }

    public void setDoaReponame(String doip_brssize) {
        setProperty(DOA_REPONAME, doip_brssize);
    }

    public String getDoaRepoid() {
        return getPropertyAsString(DOA_REPOID);
    }

    public void setDoaRepoid(String doip_brgsize) {
        setProperty(DOA_REPOID, doip_brgsize);
    }

    public String getDoaBody() {
        return getPropertyAsString(DOA_BODY);
    }

    public void setDoaBody(String body) {
        setProperty(DOA_BODY, body);
    }

    public String getDoaInitialvalue() {
        return getPropertyAsString(DOA_INITIALVALUE);
    }

    public void setDoaInitialvalue(String doip_initialvalue) {
        setProperty(DOA_INITIALVALUE, doip_initialvalue);
    }


    @Override
    public void testEnded() {
        this.testEnded("local");
    }

    @Override
    public void testEnded(String arg0) {
//		Iterator<Producer<String, String>> it = producers.values().iterator();
//		while(it.hasNext()) {
//			Producer<String, String> producer = it.next();
//			producer.close();
//		}
    }

    @Override
    public void testStarted() {
    }

    @Override
    public void testStarted(String arg0) {
    }


}
