package org.apache.jmeter.protocol.doa.control.gui;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.jmeter.gui.util.JSyntaxTextArea;
import org.apache.jmeter.gui.util.JTextScrollPane;
import org.apache.jmeter.gui.util.VerticalPanel;
import org.apache.jmeter.protocol.doa.sampler.DoaSampler;
import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jorphan.gui.JLabeledTextField;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

@SuppressWarnings("deprecation")
public class DoaSamplerUI extends AbstractSamplerGui {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggingManager.getLoggerForClass();
	private final JLabeledTextField douiagreementField = new JLabeledTextField("协议");
	private final JLabeledTextField douiipField = new JLabeledTextField("服务器名称或IP");
	private final JLabeledTextField douiportField = new JLabeledTextField("端口号");

	private final JLabeledTextField douipubkeyField = new JLabeledTextField("公钥");
	private final JLabeledTextField douiprikeyField = new JLabeledTextField("私钥");

	private final JLabeledTextField douimethodField = new JLabeledTextField("方法");
	private final JLabeledTextField douirepoNameField = new JLabeledTextField("仓库注册名字");
	private final JLabeledTextField douirepoIdField = new JLabeledTextField("仓库注册标识");

	private final JLabeledTextField douiinitialvalue = new JLabeledTextField("initialvalue");

	private final JSyntaxTextArea douibody = new JSyntaxTextArea(25, 30);
	private final JLabel textArea2 = new JLabel("Body");
	private final JTextScrollPane textPanel2 = new JTextScrollPane(douibody);

	public DoaSamplerUI() {
		super();
		this.init();
	}

	private void init(){
		log.info("Initializing the UI.");
		setLayout(new BorderLayout());
		setBorder(makeBorder());

		add(makeTitlePanel(), BorderLayout.NORTH);
		JPanel mainPanel = new VerticalPanel();
		add(mainPanel, BorderLayout.CENTER);

		JPanel D1Panel = new JPanel();
		D1Panel.setLayout(new GridLayout());
		D1Panel.add(douiagreementField);
		D1Panel.add(douiipField);
		D1Panel.add(douiportField);
		D1Panel.add(douiinitialvalue);

		JPanel ControlPanel = new VerticalPanel();
		ControlPanel.add(D1Panel);
		ControlPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "server"));
		mainPanel.add(ControlPanel);

		JPanel D2Panel = new JPanel();
		D2Panel.setLayout(new GridLayout(2,1));
		D2Panel.add(douipubkeyField);
		D2Panel.add(douiprikeyField);
		JPanel ContentPanel1 = new VerticalPanel();
		ContentPanel1.add(D2Panel);
		ContentPanel1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "repo key"));
		mainPanel.add(ContentPanel1);

		JPanel D3Panel = new JPanel();
		D3Panel.setLayout(new GridLayout());
		D3Panel.add(douimethodField);
		D3Panel.add(douirepoNameField);
		D3Panel.add(douirepoIdField);
		JPanel ContentPanel2 = new VerticalPanel();
		ContentPanel2.add(D3Panel);
		ContentPanel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "request"));
		mainPanel.add(ContentPanel2);



		JPanel Content4Panel = new VerticalPanel();
		JPanel messageContentPanel2 = new JPanel(new BorderLayout());
		messageContentPanel2.add(this.textArea2, BorderLayout.NORTH);
		messageContentPanel2.add(this.textPanel2, BorderLayout.CENTER);
		Content4Panel.add(messageContentPanel2);
		Content4Panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "parames"));
		mainPanel.add(Content4Panel);
	}

	@Override
	public TestElement createTestElement() {
		DoaSampler sampler = new DoaSampler();
		this.setupSamplerProperties(sampler);
		return sampler;
	}
	
	@Override
	public void clearGui() {
		super.clearGui();
		this.douiagreementField.setText("tcp");
		this.douiipField.setText("192.168.201.207");
		this.douiportField.setText("2041");
		this.douipubkeyField.setText("04d1c1e7ab58d8a664d575d5c46d9f55aaf6a5690955b5930b01d71f75406420c7ca5a72cc4602dd3558d893663c38252fa27eb1af1294ec66c965211b65ebd6d5");
		this.douiprikeyField.setText("a1ef6001ac670d4b56bf88464b6bf3f27e3a06b0e7c97623039c9f442b3441b5");
		this.douimethodField.setText("resolve");
		this.douirepoNameField.setText("repo1");
		this.douirepoIdField.setText("aibdbox.domain1/test01");
		this.douiinitialvalue.setText("0");
		this.douibody.setText("{\"doid\":\"bdtest5.r1/aibdtestrepo/Do.small\"}");
	}

	@Override
	public void configure(TestElement element) {
		super.configure(element);
		DoaSampler sampler = (DoaSampler)element;
		this.douiagreementField.setText(sampler.getDoaAgreement());
		this.douiipField.setText(sampler.getDoaIp());
		this.douiportField.setText(sampler.getDoaPort());
		this.douipubkeyField.setText(sampler.getDoaPubkey());
		this.douiprikeyField.setText(sampler.getDoaPrikey());
		this.douimethodField.setText(sampler.getDoaMethod());
		this.douirepoNameField.setText(sampler.getDoaReponame());
		this.douirepoIdField.setText(sampler.getDoaRepoid());
		this.douibody.setText(sampler.getDoaBody());
		this.douiinitialvalue.setText(sampler.getDoaInitialvalue());
	}

	private void setupSamplerProperties(DoaSampler sampler) {
		this.configureTestElement(sampler);
		sampler.setDoaAgreement(this.douiagreementField.getText());
		sampler.setDoaIp(this.douiipField.getText());
		sampler.setDoaPort(this.douiportField.getText());
		sampler.setDoaPubkey(this.douipubkeyField.getText());
		sampler.setDoaPrikey(this.douiprikeyField.getText());
		sampler.setDoaMethod(this.douimethodField.getText());
		sampler.setDoaReponame(this.douirepoNameField.getText());
		sampler.setDoaRepoid(this.douirepoIdField.getText());
		sampler.setDoaInitialvalue(this.douiinitialvalue.getText());
		sampler.setDoaBody(this.douibody.getText());
	}
	
	@Override
	public String getStaticLabel() {
		return "DOA Sampler";
	}

	@Override
	public String getLabelResource() {
		throw new IllegalStateException("This shouldn't be called"); 
	}

	@Override
	public void modifyTestElement(TestElement testElement) {
		DoaSampler sampler = (DoaSampler) testElement;
		this.setupSamplerProperties(sampler);
	}
}
