# BDWare 标识解析系统

标识解析系统（路由器）作为BDWare数联网基础设施的其中一个子系统，负责为数联网中所有数字对象分配全网唯一的标识，并持久地维护标识和数字对象地址的映射关系。
系统架构如图所示。

![](https://gitee.com/BDWare/BDWare/raw/master/_static/imgs/IRSArch.png)

标识解析系统具体功能包括前缀管理、后缀管理、配置管理以及标识解析。

前缀管理主要负责下一级路由器的前缀分配、更新以及删除，后缀管理主要负责当前路由器所管理的Repo的后缀分配、更新以及删除，配置管理获取当前路由器配置信息以及更新上一级路由器IP，标识解析负责路由的解析服务，主要为普通用户提供。

## 数联网中的标识

标识系统同样具有全网唯一的标识，也即“前缀”，BDWare标识系统的标识由多段组成，段与段之间以"."号分隔。
基于分段的前缀，标识系统可以采用层次化的结构管理数字对象标识，三级前缀由二级前缀分配、二级前缀可由一级前缀分配，而一级前缀由总管理员分配。

```
86           //一级前缀
86.5000      //二级前缀
86.5000.470  //三级前缀
```

数字对象标识由”前缀/后缀“组成，其中前缀代表为其分配标识的标识解析系统，而后缀则由标识解析系统保证本地唯一。

```
86.5000.470/do.test
```

## 项目说明

标识系统项目由三个部分组成

- 前端：标识系统的前端界面，提供UI工用户可视化使用。
- 后端：标识系统的服务器，存储、管理标识数据，并基于DOIRP协议提供数字对象标识的申请、解析。
- YJS：一种自研的类JS脚本语言，用于定义标识系统的管理接口，所有通过YJS定义的管理接口其访问记录均会被分布式执行环境可信存证。YJS更的多介绍文档详见![YPK示例](https://gitee.com/BDWare/contract-java-example)
### 下载项目

初次clone可使用以下命令。

```bash
git clone  https://xxxx/XXX-bundle.git
```

在git clone之后执行

```bash
git submodule update --init

```

```bash
git submodule foreach -q --recursive 'git checkout $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'
```

更新所有子项目：

```
git submodule foreach git pull --rebase origin master
git pull --rebase origin master
```

## 数联网标识解析操作

### 标识解析系统介绍

数联网路由器包括根路由器和域路由器，路由器各项属性包括创建日期、路由器名字、路由器标识、版本、路由器IP地址、路由器监听端口号、路由器审批状态、协议以及管理员公钥等。

根路由器相关属性可以在前端进行手动配置，域路由器相关信息需要从上级路由器验证获取，获取之前需要事先在上级路由器注册该路由器。

各级路由器有三项主要的功能，分别是前缀管理、后缀管理与标识解析。其中前缀管理用来管理下一级路由器，主要功能是为下一级路由器分配前缀。后缀管理用来管理Repo，主要功能是给管理的repo分配后缀。以上两项功能都需要管理员权限，只有当前路由器的管理员能够操作。标识解析功能提供给普通用户使用，普通用户可以通过标识查询相关的路由器或者repo信息。

### 根标识系统配置

下图展示了根路由器配置功能，根路由器可以在配置管理页面编辑根路由器信息。

<img src="README.assets/image-20220104144719380.png" alt="image-20220104144719380" style="zoom:80%;" />

### 子标识系统验证

下图展示了子标识系统验证功能，子标识系统需要接入上级标识系统进行验证，获取本级标识等各种信息，然后启动本级IRPServer。

<img src="README.assets/image-20220104145840742.png" alt="image-20220104145840742" style="zoom:80%;" />

### 前缀管理

下图展示了路由器前缀管理功能。路由器可以注册下一级路由器，也可以进行更新、删除、展示、查询等功能。

<img src="README.assets/image-20220104151416619.png" alt="image-20220104151416619" style="zoom:80%;" />



<img src="README.assets/image-20220104151456718.png" alt="image-20220104151456718" style="zoom:80%;" />

### 后缀管理

下图展示了路由器后缀管理功能。路由器可以注册repo，同样也有更新、删除、展示、查询注册在本级路由器的repo的功能。

<img src="README.assets/image-20220104151736039.png" alt="image-20220104151736039" style="zoom:80%;" />



<img src="README.assets/image-20220104151837598.png" alt="image-20220104151837598" style="zoom:80%;" />

### 标识解析

下图展示了路由器标识解析功能。普通用户可以使用此功能查询特定标识的信息。

<img src="README.assets/image-20220104164330565.png" alt="image-20220104164330565" style="zoom:80%;" />

## 更多文档

Docker镜像启动，详见：[部署文档](https://gitee.com/BDWare/iod-docker-deploy/blob/master/README.md)

前端开发文档，详见：[前端开发者文档](https://gitee.com/BDWare/datanet-router-front/blob/master/README.md)

后端开发文档，启动参数、接口说明、调试环境配置等，详见：[开发者文档](https://gitee.com/BDWare/datanet-router-backend/blob/master/README.md)

## 所依赖的第三方库

本系统直接依赖的第三方库均为jar包依赖，如下图所示。

| 名称                                                         | Licence类型                                                  | 说明                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | --------------------- |
| [commons-lang3](https://mvnrepository.com/artifact/org.apache.commons/commons-lang3/3.0) | [ApacheV2](https://gitee.com/link?target=https%3A%2F%2Fwww.apache.org%2Flicenses%2F) | Java工具包            |
| [nimbus-jose-jwt](https://mvnrepository.com/artifact/com.nimbusds/nimbus-jose-jwt/9.10) | [ApacheV2](https://gitee.com/link?target=https%3A%2F%2Fwww.apache.org%2Flicenses%2F) | JWT库                 |
| [cxf-core](https://mvnrepository.com/artifact/org.apache.cxf/cxf-core/3.3.4) | [ApacheV2](https://gitee.com/link?target=https%3A%2F%2Fwww.apache.org%2Flicenses%2F) | webService框架        |
| [doip-audit-tool](https://mvnrepository.com/artifact/org.bdware.doip/doip-audit-tool/1.1.2) | [Mulan PSL 2.0](https://gitee.com/link?target=http%3A%2F%2Flicense.coscl.org.cn%2FMulanPSL2) | doip协议sdk           |
| [doip-encrypt-tool](https://mvnrepository.com/artifact/org.bdware.doip/doip-encrypt-tool) | [Mulan PSL 2.0](https://gitee.com/link?target=http%3A%2F%2Flicense.coscl.org.cn%2FMulanPSL2) | doip加密sdk           |
| [sdk-java](https://gitee.com/link?target=https%3A%2F%2Fmvnrepository.com%2Fartifact%2Forg.bdware.bdcontract%2Fsdk-java%2F1.0.2) | [Mulan PSL 2.0](https://gitee.com/link?target=http%3A%2F%2Flicense.coscl.org.cn%2FMulanPSL2) | 数瑞分布式执行环境sdk |
| [cp](https://gitee.com/link?target=https%3A%2F%2Fmvnrepository.com%2Fartifact%2Forg.bdware.sc%2Fcp%2F1.6.8) | [Mulan PSL 2.0](https://gitee.com/link?target=http%3A%2F%2Flicense.coscl.org.cn%2FMulanPSL2) | 数瑞分布式执行环境    |
| [ypk-deploy-tool](https://mvnrepository.com/artifact/org.bdware.bdcontract/ypk-deploy-tool/0.7.1) | [Mulan PSL 2.0](https://gitee.com/link?target=http%3A%2F%2Flicense.coscl.org.cn%2FMulanPSL2) | bdcontract ypk部署sdk |

