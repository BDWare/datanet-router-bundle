# 测试接口

## 解析接口
解析接口示例如下：
```
http://39.104.200.8:18010/BDO?action=callBDO&shortId=GlobalRouter&operation=resolveByHTTP&arg=secourse%2F33d8f826-f6bc-44dc-86fe-a028f64b6b73
```
参数说明： 仅有arg这个参数，需要URIEncode，例如上述参数中`secourse%2F33d8f826-f6bc-44dc-86fe-a028f64b6b73`，encode前是：`secourse/33d8f826-f6bc-44dc-86fe-a028f64b6b73`。
返回结果示例：
```json
{
  "status": "Success",
  "result": {
    "owner": "04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd",
    "doId": "bdtest/e65940b6-408a-4dfe-bd2c-904ec528b707",
    "_updateTimestamp": "1680099783425_-1883116301"
  },
  "responseID": "1680101833674_653",
  "action": "onExecuteResult",
  "executeTime": "26"
}
```
## 注册接口
参数说明：
arg参数为json对象，其中handleValues为要注册的标识的内容，
sm2KeyPair为注册者的密钥对。该接口为测试接口，irp接口中密钥对不会出现在传输过程中，仅签名信息会传输。
```
http://39.104.200.8:18010/BDO?action=callBDO&shortId=GlobalRouter&operation=registerByHTTP&arg={"handleValues":{"key":"value"},"sm2KeyPair":{"publicKey":"04303718771b9323c204e607639f14469f9a94e55b0964a408ad3b3864b0493b645d7070da0d550f0c54b934275a8e88dedc3024467b0566db5c1108b1baeaae27","privateKey":"d675782acf011dbc01a73c7967ccff9564486f7c3a9f5d5de151caffaa18936"}}
```
返回结果示例：
```json
{
  "status": "Success",
  "result": "secourse/f12ddc94-fd65-4c75-acbc-98225fe902d0",
  "responseID": "1680102659954_659",
  "action": "onExecuteResult",
  "executeTime": "16"
}
```


